package com.boloutaredoubeni.myjniexample;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class JNIActivity extends AppCompatActivity {
  /**
   * This method will access our native method defined by the JNI
   */
  public native String getMyData();

  @Override
  protected void onCreate(Bundle savedInstanceState) {


    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_jni);

    setTitle(getMyData());
  }

  static {
    System.loadLibrary("mylib");
  }
}
