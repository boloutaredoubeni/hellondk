#include <string.h>

#include <jni.h>

jstring
Java_com_boloutaredoubeni_myjniexample_JNIActivity_getMyData(JNIEnv* pEnv, jobject pThis) {
  (*pEnv)->NewStringUTF(pEnv, "My native project talks C++");
}
